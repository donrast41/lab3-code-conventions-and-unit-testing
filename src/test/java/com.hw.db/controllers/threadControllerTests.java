package com.hw.db.controllers;

import com.hw.db.DAO.ThreadDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;
import com.hw.db.DAO.ForumDAO;
import com.hw.db.DAO.ThreadDAO;
import com.hw.db.DAO.UserDAO;
import com.hw.db.models.Post;
import com.hw.db.models.Thread;
import com.hw.db.models.User;
import com.hw.db.models.Vote;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.mockito.MockedStatic;
import org.mockito.Mockito;
import org.mockito.internal.verification.Times;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import java.sql.Timestamp;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

class threadControllerTests {
    private Thread thr;
    private String slug = "few";
    private String id = "2";

    @BeforeEach
    @DisplayName("Testing the thread")
    void crtThrTst() {
        thr = new Thread("thread", new Timestamp(0), "frm", "msg", slug, "hello world", 43);
    }

    @Test
    @DisplayName("ID and Slug Checker")
    void chkIdSlugTst() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadById(Integer.parseInt(id))).thenReturn(thr);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
            assertEquals(thr, controller.CheckIdOrSlug(id), "Id found");
            assertEquals(thr, controller.CheckIdOrSlug(slug), "Slug found.");

        }
    }

    @Test
    @DisplayName("Post creater")
    void crtPstTst() {
        List<Post> poststhr = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
            assertEquals(ResponseEntity.status(HttpStatus.CREATED).body(poststhr), controller.createPost(slug, poststhr));
        }
    }

    @Test
    @DisplayName("Post getter")
    void pstTst() {
        List<Post> poststhr = Collections.emptyList();
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getPosts(thr.getId(), 200, 2, null, false)).thenReturn(poststhr);
            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(poststhr), controller.Posts(slug, 200, 2, null, false));
        }
    }

    @Test
    @DisplayName("Thread changer")
    void cngTst() {
        Thread changeThread = new Thread("person", new Timestamp(2), "frm", "super msg", "changeSlug", "tpc", 4243);
        changeThread.setId(2);
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug("changeSlug")).thenReturn(changeThread);
            threadMock.when(() -> ThreadDAO.getThreadById(2)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.change("changeSlug", thr), "Change succesfull");
        }
    }

    @Test
    @DisplayName("Thread infrmation")
    void infTst() {
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            threadController controller = new threadController();

            threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);

            assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.info(slug), "Information succesfull");
        }
    }

    @Test
    @DisplayName("Vote creater")
    void crtVtTst() {
        Vote vote = new Vote("person", 5);
        User user = new User("person", "donrast42@outlook.com", "Don Rast", "R D");
        try(MockedStatic<ThreadDAO> threadMock = Mockito.mockStatic(ThreadDAO.class)) {
            try(MockedStatic<UserDAO> userMock = Mockito.mockStatic(UserDAO.class)) {
                threadController controller = new threadController();

                threadMock.when(() -> ThreadDAO.getThreadBySlug(slug)).thenReturn(thr);
                userMock.when(() -> UserDAO.Info("person")).thenReturn(user);

                assertEquals(ResponseEntity.status(HttpStatus.OK).body(thr), controller.createVote(slug, vote), "Successfull");
            }
        }
    }
}
